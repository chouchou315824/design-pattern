package com.design.pattern.creational.prototype;

import java.util.HashMap;

/**
 * @Description  基本原型模式 有三个角色  抽象原型类、 具体原型类 、 访问类
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:52
 * @ClassName
 */
public class ProtoTypeManager {

    private HashMap<String, PrototypePattern.Shape> ht = new HashMap<String, PrototypePattern.Shape>();
    public ProtoTypeManager() {
        ht.put("Circle", new PrototypePattern.Circle());
        ht.put("Square", new PrototypePattern.Square());
    }
    public void addshape(String key, PrototypePattern.Shape obj) {
        ht.put(key, obj);
    }
    public PrototypePattern.Shape getShape(String key) {
        PrototypePattern.Shape temp = ht.get(key);
        return (PrototypePattern.Shape) temp.clone();
    }



}

package com.design.pattern.creational.singleton.lazy;

/**
 * @Description  为了在多线程环境下，不影响程序的性能，不让线程每次调用方法时都加锁，而只是在实例未被创建时再加锁，在加锁处理里面还需要判断一次实例是否已存在。
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:30
 * @ClassName
 */
public class ThreadSafeSingletonTwo {
    /**
     * volatile 保证线程间的对单例对象的可见性
     */
    private static volatile  ThreadSafeSingletonTwo singleton = null;

    private ThreadSafeSingletonTwo() {
    }

    public static ThreadSafeSingletonTwo getInstance() {
        //多线程下不安全
        if (singleton == null) {
            synchronized (ThreadSafeSingletonTwo.class) {
                //外层的if 判断是多线程的 防止前两个线程都是外层都是 null 然后第一个获取锁创建了后 第二个也直接创建 依然认为外层的null 是正确的
                if (singleton == null) {
                    singleton = new ThreadSafeSingletonTwo();
                }
            }
        }
        return singleton;
    }
}

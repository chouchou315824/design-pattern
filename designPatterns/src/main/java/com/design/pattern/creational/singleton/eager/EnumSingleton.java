package com.design.pattern.creational.singleton.eager;

/**
 * @Description
 * 优点
 * 创建枚举默认就是线程安全的。
 * 防止反序列化导致重新创建新的对象。保证只有一个实例（即使使用反射机制也无法多次实例化一个枚举量）。
 * 缺点：灵活性不足
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:20
 * @ClassName
 */
public enum EnumSingleton {

    INSTANCE;

}

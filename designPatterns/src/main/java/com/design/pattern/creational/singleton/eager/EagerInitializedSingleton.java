package com.design.pattern.creational.singleton.eager;

import java.io.Serializable;

/**
 * @Description 饿汉式单例 线程安全的   调用速度快  但是 浪费资源   不使用也会创建
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:13
 * @ClassName
 */
public class EagerInitializedSingleton implements Serializable {

    private static final EagerInitializedSingleton singleton = new EagerInitializedSingleton();

    public EagerInitializedSingleton() {
    }

    public EagerInitializedSingleton getInstance() {
        return singleton;
    }

    private Object readResolve(Object object){
        return singleton;
    }

}

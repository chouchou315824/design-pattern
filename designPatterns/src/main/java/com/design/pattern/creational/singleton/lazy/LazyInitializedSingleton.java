package com.design.pattern.creational.singleton.lazy;

/**
 * @Description  多线程下不安全
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:23
 * @ClassName
 */
public class LazyInitializedSingleton {


    private static LazyInitializedSingleton singleton = null;

    private LazyInitializedSingleton() {
    }

    public static LazyInitializedSingleton getInstance(){
        //多线程下不安全
        if (singleton == null) {
            singleton = new LazyInitializedSingleton();
        }
        return singleton;
    }
}

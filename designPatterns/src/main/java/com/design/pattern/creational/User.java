package com.design.pattern.creational;

import java.io.Serializable;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/11/3 15:45
 * @ClassName
 */
public class User implements Cloneable, Serializable {


    private Integer age;
    private String name;

    private UserRelation userRelation;

    public User() {
    }

    public UserRelation getUserRelation() {
        return userRelation;
    }

    public void setUserRelation(UserRelation userRelation) {
        this.userRelation = userRelation;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public User clone() throws CloneNotSupportedException {
        User user = (User) super.clone();
        user.setUserRelation(((UserRelation) this.userRelation.clone()));
        return user;
    }

    @Override
    public String toString() {
        return "User{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", userRelation=" + userRelation +
                '}';
    }


    private User readResolve(Object object){
        return this;
    }
}

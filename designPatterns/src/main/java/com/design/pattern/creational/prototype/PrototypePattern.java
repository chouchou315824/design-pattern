package com.design.pattern.creational.prototype;

import java.util.HashMap;
import java.util.Scanner;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/11/3 18:12
 * @ClassName
 */
public class PrototypePattern {

    interface Shape extends Cloneable {
        public Object clone();    //拷贝
        public void countArea();    //计算面积
    }



    static class Circle implements Shape {
        @Override
        public Object clone() {
            Circle w = null;
            try {
                w = (Circle) super.clone();
            } catch (CloneNotSupportedException e) {
                System.out.println("拷贝圆失败!");
            }
            return w;
        }
        @Override
        public void countArea() {
            int r = 0;
            System.out.print("这是一个圆，请输入圆的半径：");
            Scanner input = new Scanner(System.in);
            r = input.nextInt();
            System.out.println("该圆的面积=" + 3.1415 * r * r + "\n");
        }
    }



    static class Square implements Shape {
        @Override
        public Object clone() {
            Square b = null;
            try {
                b = (Square) super.clone();
            } catch (CloneNotSupportedException e) {
                System.out.println("拷贝正方形失败!");
            }
            return b;
        }
        @Override
        public void countArea() {
            int a = 0;
            System.out.print("这是一个正方形，请输入它的边长：");
            Scanner input = new Scanner(System.in);
            a = input.nextInt();
            System.out.println("该正方形的面积=" + a * a + "\n");
        }
    }

    /**
     * 原型管理类
     */
   static class ProtoTypeManager {
        private HashMap<String, Shape> ht = new HashMap<String, Shape>();
        public ProtoTypeManager() {
            // 缓存 默认原型
            ht.put("Circle", new Circle());
            ht.put("Square", new Square());
        }
        // 添加原型
        public void addShape(String key, Shape obj) {
            ht.put(key, obj);
        }
        public Shape getShape(String key) {
            Shape temp = ht.get(key);
            return (Shape) temp.clone();
        }
    }
    public static void main(String[] args) {
        ProtoTypeManager pm = new ProtoTypeManager();
        //给你一个原型标识 你返回我一个原型对象
        Shape obj1 =  pm.getShape("Circle");
        obj1.countArea();
        Shape obj2 =  pm.getShape("Square");
        obj2.countArea();
    }

}

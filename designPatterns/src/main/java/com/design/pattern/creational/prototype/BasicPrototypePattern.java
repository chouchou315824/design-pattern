package com.design.pattern.creational.prototype;

import com.design.pattern.creational.UserRelation;

/**
 * @Description 原型模式
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:45
 * @ClassName
 */
public class BasicPrototypePattern {

    static interface Prototype extends Cloneable {

        Object clone();

    }

    static class PrototypeOne implements Prototype {

        private UserRelation userRelation;

        public UserRelation getUserRelation() {
            return userRelation;
        }

        public void setUserRelation(UserRelation userRelation) {
            this.userRelation = userRelation;
        }

        @Override
        public Object clone() {

            try {
                PrototypeOne prototypeOne = (PrototypeOne) super.clone();
                prototypeOne.setUserRelation((UserRelation)this.userRelation.clone());
                return prototypeOne;
            } catch (CloneNotSupportedException e) {
                return null;
            }
        }
    }

    static class PrototypeTwo implements Prototype {

        @Override
        public Object clone() {

            try {
                return super.clone();
            } catch (CloneNotSupportedException e) {
                return null;
            }
        }
    }

}

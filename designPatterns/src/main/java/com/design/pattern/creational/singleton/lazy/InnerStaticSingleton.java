package com.design.pattern.creational.singleton.lazy;

/**
 * @Description 静态内部类  外部类无法访问类中的静态类 只能通过当前类访问  访问的时候 内部类才创建  可以作为一种 懒汉式的 单例实现方式
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:34
 * @ClassName
 */
public class InnerStaticSingleton {


    @SuppressWarnings("InstantiationOfUtilityClass")
    private static class SingletonHelper{
        private static final InnerStaticSingleton SINGLETON = new InnerStaticSingleton();
    }

    public static InnerStaticSingleton getInstance(){
        return SingletonHelper.SINGLETON;
    }
}

package com.design.pattern.creational.singleton.eager;

import java.io.Serializable;

/**
 * @Description 静态代码块  实现单例模式  可以在代码块做异常处理
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:17
 * @ClassName
 */
public class StaticBlockSingleton implements Serializable {

    private static StaticBlockSingleton singleton = null;

    static {
        try {
            singleton = new StaticBlockSingleton();
        } catch (Exception e) {
            System.out.println("创建单例对象异常");
        }
    }

    private StaticBlockSingleton() {
    }

    public StaticBlockSingleton getInstance() {
        return singleton;
    }

    private Object readResolve(Object object) {
        return singleton;
    }
}

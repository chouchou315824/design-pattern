package com.design.pattern.creational.singleton.lazy;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/11/3 17:30
 * @ClassName
 */
public class ThreadSafeSingletonOne {

    private static ThreadSafeSingletonOne singleton = null;

    private ThreadSafeSingletonOne() {
    }

    public static synchronized ThreadSafeSingletonOne getInstance(){
        //多线程下不安全
        if (singleton == null) {
            singleton = new ThreadSafeSingletonOne();
        }
        return singleton;
    }
}

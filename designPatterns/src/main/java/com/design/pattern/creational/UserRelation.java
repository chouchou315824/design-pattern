package com.design.pattern.creational;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/11/3 15:44
 * @ClassName
 */
public class UserRelation implements Cloneable {
    private String parent;



    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "UserRelation{" +
                "parent='" + parent + '\'' +
                '}';
    }
}

package com.design.principle.lsp;

/**
 * @Description 里氏替换原则  在子类中尽量不要重写父类的方法  如何更好的使用继承
 * 通过可以抽象出更通俗的基类来供原来的子类和父类继承，如果原来的子类还需要原来父类的功能，可以采用依赖、聚合、组合等 弱耦合的关系来实现
 * @Author houchenkai
 * @Created Date: 2021/11/2 15:37
 * @ClassName
 */
public class WrongSample {


    public static void main(String[] args) {
        A a = new A();
        a.add(1, 2);
        A b = new B();
        // 因为 b误重写了A的方法 本来想要的是A的相加的功能 结果A 不知道 变成了 相减
        b.add(1, 2);
    }


    /**
     * 计算两数相加 相乘
     */
    static class A {

        public int add(int numOne, int twoOne) {

            return numOne + twoOne;
        }

        /**
         * 乘
         *
         * @param numOne
         * @param twoOne
         * @return
         */
        public int multiply(int numOne, int twoOne) {

            return numOne * twoOne;
        }
    }

    /**
     * 计算两数相加 相除
     */
    static class B extends A {
        @Override
        public int add(int numOne, int twoOne) {
            return numOne - twoOne;
        }

        /**
         * 除
         *
         * @param numOne
         * @param twoOne
         * @return
         */
        public int division(int numOne, int twoOne) {

            return numOne / twoOne;
        }

    }

}

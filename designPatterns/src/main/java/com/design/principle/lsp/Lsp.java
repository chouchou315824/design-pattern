package com.design.principle.lsp;

/**
 * @Description 里氏替换原则  在子类中尽量不要重写父类的方法  如何更好的使用继承
 * 通过可以抽象出更通俗的基类来供原来的子类和父类继承，如果原来的子类还需要原来父类的功能，可以采用依赖、聚合、组合等 弱耦合的关系来实现
 * @Author houchenkai
 * @Created Date: 2021/11/2 16:06
 * @ClassName
 */
public class Lsp {


    public static void main(String[] args) {
        A a = new A();
        a.add(1,2);
        a.multiply(1,2);
        B b = new B();
        b.add(1,2);
        b.division(1,2);
    }


     static abstract class BasicCalculate{
         public int add(int numOne, int twoOne) {
             return numOne + twoOne;
         }

    }


    static class A extends BasicCalculate {

        /**
         * 乘
         *
         * @param numOne
         * @param twoOne
         * @return
         */
        public int multiply(int numOne, int twoOne) {

            return numOne * twoOne;
        }
    }

    /**
     * 计算两数相加 相除
     */
    static class B extends BasicCalculate {

        /**
         * 除
         *
         * @param numOne
         * @param twoOne
         * @return
         */
        public int division(int numOne, int twoOne) {

            return numOne / twoOne;
        }

    }

}

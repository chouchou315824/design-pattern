package com.design.principle.ocp;

/**
 * @Description 开闭原则  提供方/对外开放方法功能 =》 对扩展开放     使用方/调用开放方法功能  =》对修改关闭
 *              实现思路：抽象提供方的功能  使用方只需要使用抽象 不关心具体的内容
 * @Author houchenkai
 * @Created Date: 2021/11/2 15:21
 * @ClassName
 */
public class Ocp {


    public static void main(String[] args) {
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShape(new Rectangle());
        graphicEditor.drawShape(new CirCle());
        graphicEditor.drawShape(new Triangle());
    }

    /**
     * 图形编辑器  使用方 对修改关闭  不用关心细节
     */
    static class GraphicEditor {
        /**
         * 使用画图形的功能
         * @param shape
         */
        public void drawShape(Shape shape){
            // 作为使用方 对修改关闭  对扩展不关心
            shape.draw();
        }

    }



    abstract static  class Shape {
        /**
         * 对外提供画不同形状的功能
         */
      abstract void draw();

    }

    static class Rectangle extends Shape {
        @Override
        void draw() {

        }
    }

    static class CirCle extends Shape {
        @Override
        void draw() {

        }
    }

    /**
     * 新增 画三角形的需求
     */
    static class Triangle extends Shape {
        @Override
        void draw() {

        }
    }

}

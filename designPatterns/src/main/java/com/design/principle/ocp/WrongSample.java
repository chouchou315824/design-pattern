package com.design.principle.ocp;

/**
 * @Description 开闭原则的错误示例 ： 实现一个 图形编辑器画不同形状图形的功能
 * 需求分析：  提供一个想要的图形 图形编辑器 返回具体的图形
 * @Author houchenkai
 * @Created Date: 2021/11/2 14:57
 * @ClassName
 */
public class WrongSample {

    public static void main(String[] args) {
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShape(new Rectangle());
        graphicEditor.drawShape(new CirCle());
        graphicEditor.drawShape(new Triangle());

        //存在问题  图形编辑器 可能不单单存在画图形的功能， 图形编辑器作为 画图具体功能的调用方/使用方，应该对修改关闭
        //         画图形的功能 对外提供画不同形状图形的功能/提供方， 应该对扩展开放   支持多个软件实体扩展功能，在功能扩展时尽量不修改源代码
        //         目前如果需要扩展 使用方需要修改，提供方也需要修改

    }

    /**
     * 图形编辑器   使用方  使用画不同形状的功能
     */
    static class GraphicEditor {
        /**
         * 画图形
         * @param shape
         */
        public void drawShape(Shape shape) {
            if (shape.type == 1) {
                drawRectangle(shape);
            } else if (shape.type == 2) {
                drawCirCle(shape);
            } else if (shape.type == 3) {
                drawTriangle(shape);
            }
        }

        private void drawRectangle(Shape shape) {
            System.out.println("绘制矩形");
        }

        private void drawCirCle(Shape shape) {
            System.out.println("绘制圆形");
        }

        private void drawTriangle(Shape shape) {
            System.out.println("绘制三角形");
        }

    }


    // 画具体图形的组件

    /**
     * 图形基类  画不同图形的功能
     */
    static class Shape {
        int type;
    }

    static class Rectangle extends Shape {
        Rectangle() {
            super.type = 1;
        }
    }

    static class CirCle extends Shape {
        CirCle() {
            super.type = 2;
        }
    }

    /**
     * 新增 画三角形的需求
     */
    static class Triangle extends Shape {
        Triangle() {
            super.type = 3;
        }
    }


}

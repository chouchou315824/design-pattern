package com.design.principle.lod;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description 迪米特原则 ； 最少知道原则  尽量减少类与类之间的依赖/耦合关系（一个类中出现的其他类都是有依赖或者耦合关系的） 只和直接朋友有依赖关系，减少不必要的依赖、耦合
 * Employee（学校总部员工类）、CollegeEmployee（学院的员工类）、CollegeManager（管理学院员工的管理类）、SchoolManager（学校管理类）
 * 需求：打印学校所有员工的id
 * @Author houchenkai
 * @Created Date: 2021/11/3 10:36
 * @ClassName
 */
public class Lod {


    public static void main(String[] args) {
        SchoolManager schoolManager = new SchoolManager();
        CollegeManager collegeManager = new CollegeManager();
        schoolManager.printAllEmployee(collegeManager);
    }

    static class Employee {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    static class CollegeEmployee {
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    static class CollegeManager {
        static List<CollegeEmployee> collegeEmployeeList = new ArrayList<>();

        static {
            for (int i = 0; i < 5; i++) { //这里我们增加了5个员工到 list
                CollegeEmployee collegeEmployee = new CollegeEmployee();
                collegeEmployee.setId("学院总部员工id= " + i);
                collegeEmployeeList.add(collegeEmployee);
            }
        }

        public void  printCollegeEmploy() {
            for (CollegeEmployee collegeEmployee : collegeEmployeeList) {
                System.out.println(collegeEmployee.getId());
            }
        }
    }

    static class SchoolManager {
        // 学校总部的员工
        static List<Employee> schoolEmployeeList = new ArrayList<>();

        static {
            for (int i = 0; i < 5; i++) { //这里我们增加了5个员工到 list
                Employee emp = new Employee();
                emp.setId("学校总部员工id= " + i);
                schoolEmployeeList.add(emp);
            }
        }

        //该方法完成输出学校总部和学院员工信息(id)
        void printAllEmployee(CollegeManager collegeManager) {
            //获取到学院员工
            System.out.println("------------学院员工------------");
            collegeManager.printCollegeEmploy(); //学校管理 和 学院管理是直接朋友 不和其他的类 有 依赖关系 耦合

            //获取到学校总部员工
            System.out.println("------------学校总部员工------------");
            for (Employee e : schoolEmployeeList) {
                System.out.println(e.getId());
            }
        }

    }
}

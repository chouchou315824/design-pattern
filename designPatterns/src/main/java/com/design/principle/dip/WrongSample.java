package com.design.principle.dip;

/**
 * @Description  依赖倒置原则， 对于依赖的 不依赖细节 依赖抽象  依赖关系中相互依赖其抽象 不应该依赖细节
 * @Author houchenkai
 * @Created Date: 2021/11/2 15:37
 * @ClassName
 */
public class WrongSample {


    public static void main(String[] args) {
        ChinaPerson chinaPerson = new ChinaPerson();
        //中国人接受消息 依赖了 微信 接受消息
        //如果依赖邮件、QQ等 需要进行修改 人需要新增 一个接受邮件消息的方法
        chinaPerson.receive(new WeChatMessage());
    }


    static class ChinaPerson{

        private void receive (WeChatMessage weChatMessage){
            weChatMessage.getInfo();
        }

    }

    /**
     * 消息
     */
    static class WeChatMessage{

        private void getInfo(){
            System.out.println("接受微信消息");
        }

    }


}

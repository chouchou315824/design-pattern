package com.design.principle.dip;

/**
 * @Description  依赖倒置原则   以依赖关系不能直接依赖细节  应该依赖其抽象
 *  核心理念： 基于抽象设计的框架比 依赖细节的框架要稳定的多 可扩展性强
 *  核心思想： 面向接口编程， 依赖直之间的传递可以通过  局部变量  方法入参 继承等
 * @Author houchenkai
 * @Created Date: 2021/11/2 16:55
 * @ClassName
 */
public class Dip {

    public static void main(String[] args) {
        Person person = new ChinaPerson();
        person.receive(new WeChatMessage());
        person.receive(new EmailMessage());

        // 长虹电视 通过 长虹遥控器 开机 播放
        // 电视 依赖 遥控器 发送指令 然后开机 播放
        // 如果需要新增的智能遥控器 开长虹电视  则只需要新增 遥控器实例
        // 遥控器依赖电视 电视依赖遥控器  不再依赖具体的实现
        ITv iTv = new ChangHongTv();
        RemoteControl remoteControl = new ChangHongRemoteControl();
        remoteControl.sendOpen(iTv);
        iTv.play();
    }


    interface Person {

        void receive(MessageProvider messageProvider);

    }

    static class ChinaPerson implements Person {
        @Override
        public void receive(MessageProvider messageProvider) {
            messageProvider.getInfo();
        }
    }

    interface MessageProvider {
        /**
         * 展示信息
         */
        void getInfo();
    }

    /**
     * 消息
     */
    static class WeChatMessage implements MessageProvider {

        @Override
        public void getInfo() {
            System.out.println("接受微信消息");
        }
    }

    /**
     * 消息
     */
    static class EmailMessage implements MessageProvider {

        @Override
        public void getInfo() {
            System.out.println("接受邮箱消息");
        }
    }

    /**
     * 电视  功能 播放
     */
    interface  ITv{

      void play();

      void receiveCommand(String command);

    }

    /**
     * 遥控器  功能 开 关
     */
    interface RemoteControl{
        /**
         * 对电视开启发送指令
         * @param iTv
         */
        void sendOpen(ITv iTv);
        /**
         * 对电视关闭发送指令
         * @param iTv
         */
        void sendClose(ITv iTv);

    }

   static class ChangHongTv implements ITv{
        @Override
        public void play() {
            System.out.println("电视播放");
        }

       @Override
       public void receiveCommand(String command) {
           System.out.println("处理指令"+command);
       }
   }

    static class ChangHongRemoteControl implements RemoteControl{
        @Override
        public void sendOpen(ITv iTv) {
            iTv.receiveCommand("开机。。。。");
        }

        @Override
        public void sendClose(ITv iTv) {
            iTv.receiveCommand("关机。。。。");
        }
    }

    /**
     * 新增智能遥控器
     */
    static class IntelligentRemoteControl implements RemoteControl{
        @Override
        public void sendOpen(ITv iTv) {
            iTv.receiveCommand("开机。。。。");
        }

        @Override
        public void sendClose(ITv iTv) {
            iTv.receiveCommand("关机。。。。");
        }
    }
}

package com.design;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/11/1 14:10
 * @ClassName
 */
@SpringBootApplication
public class DesignPatternWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(DesignPatternWebApplication.class,args);
    }
}
